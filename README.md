# VintraTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.6.

## Prerequisites

* Install last node version in order to get npm. 
* Install project dependencies, executing `npm run install`
* Install docker and clone required image
* run docker

## Development server

After installing and running required docker image, execute `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Important things

### Reactive Application

THis application is a full implementation of the reactive manifesto. CHeck more info clicking here: https://www.reactivemanifesto.org/

### Application structure

Code is structured by contexts: 
* **controllers**: contains the application controller
* **definitions**: important definitions for the application
* **guards**: navigation security utils, based on user state
* **interceptors**: contains JWT interceptor
* **models**: hosts application model and dto definitions
* **services**: all services are declared there
* **utils**
* **login**: the login component
* **home**: it renders sensor information and logged user data


### MVC

All this application is build following the Model-View-Controller pattern. For any enterprise-based project is recommended to follow any pattern or best practice like MVC


### Message Bus pattern

The application bases its communication on this enterprise pattern. 

There is a global class, called MessageBus, that implements this pattern reactively. 

Any entity can send messages through this bus, and any subscriptor can react to these messages.

This is a clean solution to communicate the whole application, promoting separation of concerns, high cohesion and low coupling.


### Application model

the AppModel class implements a reactive model as an Application Model Locator (another enterprise pattern).
This model, as a singletton being, can be injected any place we need, and we can react to its changes.

An important thing is the jwt token is not persistent in any accessible place, like local storage, for example, is kept in memory and removed when application shut down.

### JWT interceptor

This important piece applies an AOP (Aspect Orientation Programming) in order to attach the token to any request. The login request is the only one not including this token.

This process is applied transparently.

### Lazy load

All important components (login and Home) are lazy loaded, it means that only required is loaded. When we navigate to home (after a successfully login), login component is detached from memory and home is loaded, in order to minimize required frontend memory.

