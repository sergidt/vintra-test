import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppController } from './controllers/app.controller';
import { SystemMessageActions } from './definitions/system-message-actions';
import { MessageBus } from './services/message-bus/message-bus.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor(private controller: AppController, private snackBar: MatSnackBar,
        private messageBus: MessageBus) {
    }

    ngOnInit() {
        this.messageBus.observe(SystemMessageActions.ShowToastMessage)
            .subscribe(({ data }) => this.showToastMessage(data));
    }

    private showToastMessage(message: string) {
        this.snackBar.open(message, null, {
            duration: 2000,
        });
    }
}
