import { Inject, Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { AppEndpoints, APPLICATION_END_POINTS } from '../definitions/endpoints';
import { Logger } from './logger';

@Injectable({
    providedIn: 'root'
})
export class API {
    public readonly endPoints: AppEndpoints;

    private API_URL: string;

    constructor(@Inject(APPLICATION_END_POINTS) _endPoints: AppEndpoints) {
        this.setApiUrl();
        this.endPoints = _endPoints;

        Logger.info(this, 'Init. Parameters: ', {
            apiURL: this.API_URL,
            endPoints: this.endPoints
        });
    }

    setApiUrl() {
        this.API_URL = `${ environment.host }:${ environment.port }`;
    }

    resolve(url: string, params?) {
        if (!params)
            return `${ this.API_URL }${ url }`;

        const resolved = Object.keys(params).reduce((acc, param) => acc.replace(`:${ param }`, params[param]), url);
        return `${ this.API_URL }${ resolved }`;
    }
}
