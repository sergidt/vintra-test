import { HttpClient } from '@angular/common/http';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Sensor } from '../models/dtos';
import { API } from './api';

@Injectable({
    providedIn: 'root'
})
export class SensorService {

    constructor(private http: HttpClient, @Inject(forwardRef(() => API)) private api: API) {
    }

    getSensorList(): Observable<Array<Sensor>> {
        return this.http.get<Array<Sensor>>(this.api.resolve(this.api.endPoints.api.sensors));
    }

    getSensor(sensorId: number): Observable<Sensor> {
        return this.http.get<Sensor>(this.api.resolve(this.api.endPoints.api.sensorById, { sensorId }));
    }

    upsertSensor(sensor: Sensor): Observable<Sensor> {
        return !!sensor.id
            ? this.http.put<Sensor>(this.api.resolve(this.api.endPoints.api.sensorById, { sensorId: sensor.id }), sensor) // update sensor
            : this.http.post<Sensor>(this.api.resolve(this.api.endPoints.api.sensors), sensor); // insert sensor
    }

    deleteSensor(sensorId: number): Observable<any> {
        return this.http.delete<Sensor>(this.api.resolve(this.api.endPoints.api.sensorById, { sensorId }));
    }
}
