import { HttpClient, HttpParams } from '@angular/common/http';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Health, LoginData, LoginResponse, User } from '../models/dtos';
import { API } from './api';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private httpClient: HttpClient, @Inject(forwardRef(() => API)) private api: API) {
    }

    getHealth(): Observable<Health> {
        return this.httpClient.get<Health>(this.api.resolve(this.api.endPoints.health));
    }

    login(loginData: LoginData): Observable<LoginResponse> {
        const body = new HttpParams()
            .set('username', loginData.username)
            .set('password', loginData.password);

        return this.httpClient.post<LoginResponse>(this.api.resolve(this.api.endPoints.auth.login),
            body.toString(),
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                }
            }
        );
    }

    getAuthenticatedUser(): Observable<User> {
        return this.httpClient.get<User>(this.api.resolve(this.api.endPoints.auth.me));
    }
}
