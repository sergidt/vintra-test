export class Logger {
    public static info(source: any, message: any, params: any = '', color: string = 'inherit'): void {
        Logger.trace(source, message, params, color, 'info');
    }

    public static debug(source: any, message: any, params: any = '', color: string = 'inherit'): void {
        Logger.trace(source, message, params, color, 'debug');
    }

    public static warn(source: any, message: any, params: any = '', color: string = 'inherit'): void {
        Logger.trace(source, message, params, color, 'warn');
    }

    public static error(source: any, message: any, params: any = '', color: string = 'inherit'): void {
        Logger.trace(source, message, params, color, 'error');
    }

    public static log(...params): void {
        console.log(...(params ? params : []));
    }

    private static trace(source: any, message: any, params: any = '', color: string = 'inherit', logFunction: string): void {
        console[logFunction](`%c${ this.getSourceName(source) } %c${ message }`,
            `color: ${ color }`, `color: inherit`, params, Logger.getNow());
    }

    public static group(source: any, messageGroup: object, type: string = null): void {
        if (type) {
            console.groupCollapsed(`${ this.getSourceName(source) } -> ${ type } ${ this.getNow() }`);
        } else {
            console.groupCollapsed(`${ this.getSourceName(source) } ${ this.getNow() }`);
        }

        for (const property in messageGroup) {
            if (!messageGroup.hasOwnProperty(property)) continue;

            console.log(`${ property } :`, messageGroup[property]);
        }

        console.groupEnd();
    }

    private static getNow(): string {
        return `@ ${ new Date().toLocaleString() }`;
    }

    private static getSourceName(source: any = 'to-be-set'): string {
        return (typeof source === 'string') ? `(${ source })` : `[${ source.constructor['name'] }]`;
    }
}
