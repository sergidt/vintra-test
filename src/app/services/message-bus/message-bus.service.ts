import { forwardRef, Inject, Injectable } from '@angular/core';

import { Observable, of, Subject } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { Logger } from '../logger';
import { SystemMessage, SystemMessageAction } from './definitions';

@Injectable({
    providedIn: 'root'
})
export class MessageBus extends Subject<SystemMessage> {
    constructor() {
        super();
        Logger.info(this, 'Init');
        this.subscribe((message: SystemMessage) => Logger.group(this, message, message.type));
    }

    destroy() {
        Logger.info(this, 'Destroy');
        this.complete();
    }

    observe = (type: SystemMessageAction): Observable<SystemMessage> => this.pipe(filter(msg => msg.type === type));
    observeOnce = (type: SystemMessageAction): Observable<SystemMessage> => this.observe(type).pipe(first());
    observeGroup = (types: Array<SystemMessageAction>): Observable<SystemMessage> => this.pipe(filter(msg => types.includes(msg.type)));

    push(message: SystemMessage): Observable<SystemMessage> {
        this.next(message);
        return of(message);
    }
}
