export type SystemMessageAction = string;

export interface SystemMessage<T = any> {
    type: SystemMessageAction;
    data?: T;
    callback?: (...params) => any;
}

