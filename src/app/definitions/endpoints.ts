import { InjectionToken } from '@angular/core';

export const APPLICATION_END_POINTS = new InjectionToken('APPLICATION_END_POINTS');

export interface AppEndpoints {
    health: string;
    auth: {
        login: string;
        me: string;
    };
    api: {
        sensors: string;
        sensorById: string;
    };
}

export const ApplicationEndpoints: AppEndpoints = {
    health: '/health',
    auth: {
        login: '/auth/login',
        me: '/auth/me'
    },
    api: {
        sensors: '/api/v1/sensors',
        sensorById: '/api/v1/sensors/:sensorId'
    }
};
