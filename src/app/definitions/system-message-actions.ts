export enum SystemMessageActions {
    Login = 'Login',
    GetAuthenticatedUser = 'GetAuthenticatedUser',
    GetSensorList = 'GetSensorList',
    UpdateSensor = 'UpdateSensor',
    CreateSensor = 'CreateSensor',
    DeleteSensor = 'DeleteSensor',
    ShowToastMessage = 'ShowToastMessage',
}
