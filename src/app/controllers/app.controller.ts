import { Injectable, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionLike } from 'rxjs';
import { exhaustMap, switchMap, tap } from 'rxjs/operators';
import { SystemMessageActions } from '../definitions/system-message-actions';
import { AppModel } from '../models/app.model';
import { LoginSuccess, User } from '../models/dtos';
import { AuthService } from '../services/auth.service';
import { Logger } from '../services/logger';
import { MessageBus } from '../services/message-bus/message-bus.service';
import { SensorService } from '../services/sensor.service';

@Injectable({
    providedIn: 'root'
})
export class AppController implements OnDestroy {
    private _subscriptions: Array<SubscriptionLike>;

    constructor(private auth: AuthService,
        private sensorService: SensorService,
        private model: AppModel,
        private messageBus: MessageBus,
        private router: Router) {
        Logger.info(this, 'Init');
        this.init();
    }

    private init() {
        this._subscriptions = [
            this.auth.getHealth()
                .subscribe(_ => this.model.updateProperty('health', _)),

            this.messageBus.observe(SystemMessageActions.Login)
                .subscribe(({ data }) => {
                    this.storeToken(data);
                    this.router.navigate(['/home']);
                }),

            this.messageBus.observe(SystemMessageActions.GetAuthenticatedUser)
                .pipe(switchMap(() => this.auth.getAuthenticatedUser()))
                .subscribe((user: User) => this.model.updateProperty('user', user)),

            this.messageBus.observe(SystemMessageActions.GetSensorList)
                .pipe(exhaustMap(() => this.sensorService.getSensorList()))
                .subscribe(_ => this.model.updateProperty('sensors', _)),

            this.messageBus.observeGroup([SystemMessageActions.CreateSensor, SystemMessageActions.UpdateSensor])
                .pipe(
                    exhaustMap(({ data }) => this.sensorService.upsertSensor(data)),
                    tap(() => this.messageBus.push({ type: SystemMessageActions.ShowToastMessage, data: 'Sensor upsert' }))
                )
                .subscribe(() => this.messageBus.push({ type: SystemMessageActions.GetSensorList })),

            this.messageBus.observe(SystemMessageActions.DeleteSensor)
                .pipe(exhaustMap(
                    ({ data }) => this.sensorService.deleteSensor(data)),
                    tap(() => this.messageBus.push({ type: SystemMessageActions.ShowToastMessage, data: 'Sensor deleted' })))
                .subscribe(() => this.messageBus.push({ type: SystemMessageActions.GetSensorList })),
        ];
    }

    private storeToken(loginResponse: LoginSuccess) {
        this.model.updateProperties([['access_token', loginResponse.access_token], ['token_type', loginResponse.token_type]]);
        this.router.navigate(['/home']);
    }

    ngOnDestroy() {
        this._subscriptions.forEach(_ => _.unsubscribe());
        Logger.info(this, 'Destroyed');
    }
}
