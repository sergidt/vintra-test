import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { distinctUntilChanged, pluck } from 'rxjs/operators';
import { Logger } from '../services/logger';
import { Health, Sensor, User } from './dtos';

export interface AppModelState {
    health?: Health;
    access_token?: string;
    token_type?: string;
    user?: User;
    sensors?: Array<Sensor>;
}

const INITIAL_MODEL_STATE: AppModelState = {};

@Injectable({
    providedIn: 'root'
})
export class AppModel extends BehaviorSubject<AppModelState> {

    constructor() {
        super(INITIAL_MODEL_STATE);
        Logger.info(this, 'Init');
        this.subscribe(_ => Logger.info(this, 'Current state', _));
    }

    update(value: AppModelState): void {
        this.next(value);
    }

    updateProperty<K extends keyof AppModelState>(propertyName: K, value: any): Observable<any> {
        const currentValue: AppModelState = Object.assign({}, this.getCurrentValue());
        currentValue[propertyName] = value;
        this.update(currentValue);
        return of(value);
    }

    removeProperty<K extends keyof AppModelState>(propertyName: K): void {
        const currentValue: AppModelState = Object.assign({}, this.getCurrentValue());
        delete currentValue[propertyName];
        this.update(currentValue);
    }

    updateProperties<K extends keyof AppModelState>(props: Array<[K, any]>): void {
        const currentValue: AppModelState = Object.assign({}, this.getCurrentValue());
        props.forEach(([k, v]: [K, any]) => (currentValue[k] = v));
        this.update(currentValue);
    }

    get model$(): Observable<AppModelState> {
        return this.asObservable();
    }

    getPropertyValue(...keys): any {
        return keys.reduce((value, currentKey) => (!!value ? value[currentKey] : value), this.value);
    }

    getPropertiesValue(props: Array<Array<string>>): any {
        const result = {};
        props.forEach(keys => result[keys[keys.length - 1]] = this.getPropertyValue(...keys));
        return result;
    }

    getCurrentValue(): AppModelState {
        return this.value;
    }

    getProperty(...keys): Observable<any> {
        return this.pipe(pluck(...keys), distinctUntilChanged());
    }

    destroy() {
        Logger.info(this, 'Destroy');
        this.complete();
    }
}
