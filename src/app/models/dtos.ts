import { HttpErrorResponse } from '@angular/common/http';

export interface Health {
    pid: number;
    rss_bytes: number;
    num_connections: number;
    cpu_percent: string;
    db_status_ok: boolean;
    num_users: number;
}

export interface LoginData {
    username: string;
    password: string;
}

export interface LoginSuccess {
    access_token: string;
    token_type: string;
}

export interface RequestError {
    detail: Array<{
        loc: Array<string>;
        msg: string;
        type: string;
    }>;
}

export type LoginResponse = LoginSuccess | RequestError | HttpErrorResponse;

export interface User {
    id: number;
    username: string;
    full_name: string;
    email: string;
    disabled: boolean;
}

export interface Sensor {
    id: number;
    description: string;
    samplingPeriod: number;
    isActive: boolean;
}


