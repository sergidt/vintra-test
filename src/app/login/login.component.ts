import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AppEndpoints, APPLICATION_END_POINTS } from '../definitions/endpoints';
import { SystemMessageActions } from '../definitions/system-message-actions';
import { AppModel } from '../models/app.model';
import { Health, LoginSuccess } from '../models/dtos';
import { AuthService } from '../services/auth.service';
import { SystemMessage } from '../services/message-bus/definitions';
import { MessageBus } from '../services/message-bus/message-bus.service';
import { filterNotNulls } from '../utils/utils';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    error = '';
    healthStatus: string;

    constructor(private fb: FormBuilder,
        public model: AppModel,
        private auth: AuthService,
        private messageBus: MessageBus,
        private cd: ChangeDetectorRef,
        @Inject(APPLICATION_END_POINTS) private endPoints: AppEndpoints) {
    }

    ngOnInit(): void {
        this.loginForm = this.fb.group({
            username: ['test18', [Validators.required]],
            password: ['1234', Validators.required],
        });

        this.model.getProperty('health')
            .pipe(filterNotNulls())
            .subscribe((health: Health) => {
                this.healthStatus = 'DB Status: ' + (health.db_status_ok ? 'OK' : 'KO');
                this.cd.markForCheck();
            });
    }

    login() {
        this.auth.login(this.loginForm.getRawValue())
            .pipe(
                catchError((error, _) => {
                    this.error = error.error?.detail || error.message;
                    this.cd.detectChanges();
                    return throwError(error);
                }))
            .subscribe((_: LoginSuccess) => this.messageBus.push({
                type: SystemMessageActions.Login,
                data: _
            } as SystemMessage<LoginSuccess>));
    }
}
