import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';


// REACTIVE OPERATORS

/**
 * Filters ignoring null values
 */
export const filterNotNulls = <T>() => (source$: Observable<T>): Observable<T> =>
    source$.pipe(filter(x => x !== null && x !== undefined));
