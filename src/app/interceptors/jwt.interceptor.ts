import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppModel } from '../models/app.model';
import { Logger } from '../services/logger';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private model: AppModel) {
        Logger.info(this, 'Init');
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available

        const { token_type, access_token } = this.model.getCurrentValue();
        if (token_type && access_token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `${ token_type } ${ access_token }`
                }
            });
        }
        return next.handle(request);
    }
}
