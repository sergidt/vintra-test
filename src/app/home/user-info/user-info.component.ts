import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AppModel } from '../../models/app.model';

@Component({
    selector: 'app-user-info',
    templateUrl: './user-info.component.html',
    styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
    readonly userForm: FormGroup;

    constructor(public model: AppModel, private cd: ChangeDetectorRef, fb: FormBuilder) {
        this.userForm = fb.group({
            id: '',
            username: '',
            full_name: '',
            email: '',
            disabled: false
        });
    }

    ngOnInit(): void {
        this.model.getProperty('user')
            .pipe(first())
            .subscribe(_ => {
                this.userForm.patchValue(_);
                this.cd.markForCheck();
            });
    }

}
