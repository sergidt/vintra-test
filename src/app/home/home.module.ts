import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { DeleteSensorComponent } from './sensor-list/delete-sensor/delete-sensor.component';
import { SensorDetailComponent } from './sensor-list/sensor-detail/sensor-detail.component';
import { SensorListComponent } from './sensor-list/sensor-list.component';
import { UserInfoComponent } from './user-info/user-info.component';

@NgModule({
    declarations: [
        HomeComponent,
        UserInfoComponent,
        SensorListComponent,
        SensorDetailComponent,
        DeleteSensorComponent
    ],
    imports: [HomeRoutingModule,
              MatToolbarModule,
              MatButtonModule,
              MatIconModule,
              MatTabsModule,
              ReactiveFormsModule,
              MatFormFieldModule,
              MatInputModule,
              MatTableModule,
              MatPaginatorModule,
              MatCheckboxModule,
              MatBottomSheetModule,
              MatSortModule
    ],
    exports: [HomeComponent]
})
export class HomeModule {
}
