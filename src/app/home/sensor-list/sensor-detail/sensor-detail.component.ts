import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { SensorService } from '../../../services/sensor.service';

@Component({
    selector: 'app-sensor-detail',
    templateUrl: './sensor-detail.component.html',
    styleUrls: ['./sensor-detail.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SensorDetailComponent implements OnInit {
    sensorForm: FormGroup;

    constructor(private fb: FormBuilder, private sensorService: SensorService,
        private bottomSheetRef: MatBottomSheetRef<SensorDetailComponent>) {
    }

    @Input()
    set sensorId(sensorId: number) {
        if (!!sensorId)
            this.sensorService.getSensor(sensorId)
                .subscribe(sensor => this.sensorForm.patchValue(sensor));
    }

    ngOnInit(): void {
        this.sensorForm = this.fb.group({
            id: null,
            description: ['', [Validators.required]],
            samplingPeriod: [null, [Validators.required, Validators.min(5)]],
            isActive: false
        });
    }

    saveSensor() {
        this.bottomSheetRef.dismiss(this.sensorForm.getRawValue());
    }

    close() {
        this.bottomSheetRef.dismiss(null);
    }
}
