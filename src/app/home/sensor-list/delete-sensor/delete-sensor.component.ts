import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';

@Component({
    selector: 'app-delete-sensor',
    templateUrl: './delete-sensor.component.html',
    styleUrls: ['./delete-sensor.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteSensorComponent {

    @Input() sensorId: number;

    constructor(private bottomSheetRef: MatBottomSheetRef<DeleteSensorComponent>) {
    }

    close(deleteSensor: boolean = false) {
        this.bottomSheetRef.dismiss(deleteSensor);
    }
}
