import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { SystemMessageActions } from '../../definitions/system-message-actions';
import { AppModel } from '../../models/app.model';
import { Sensor } from '../../models/dtos';
import { SystemMessage } from '../../services/message-bus/definitions';
import { MessageBus } from '../../services/message-bus/message-bus.service';
import { filterNotNulls } from '../../utils/utils';
import { DeleteSensorComponent } from './delete-sensor/delete-sensor.component';
import { SensorDetailComponent } from './sensor-detail/sensor-detail.component';

@Component({
    selector: 'app-sensor-list',
    templateUrl: './sensor-list.component.html',
    styleUrls: ['./sensor-list.component.scss']
})
export class SensorListComponent implements AfterViewInit, OnInit, OnDestroy {
    subscription: Subscription;

    displayedColumns: string[] = ['select', 'id', 'description', 'samplingPeriod', 'isActive'];
    dataSource = new MatTableDataSource<Sensor>([]);
    selection = new SelectionModel<Sensor>(false, []);

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(public model: AppModel,
        private cd: ChangeDetectorRef,
        private bottomSheet: MatBottomSheet,
        private messageBus: MessageBus) {
    }

    ngAfterViewInit(): void {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

    }

    ngOnInit() {
        this.subscription = this.model.getProperty('sensors')
                                .pipe(filterNotNulls())
                                .subscribe(_ => {
                                    this.dataSource.data = _;
                                    this.cd.markForCheck();
                                });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    createOrEditSensor(sensorId: number = null) {
        const componentRef = this.bottomSheet.open(SensorDetailComponent);
        componentRef.instance.sensorId = sensorId;
        componentRef.afterDismissed()
                    .pipe(take(1))
                    .subscribe((sensor: Sensor) => {
                        if (!!sensor) {
                            this.messageBus.push({
                                type: !!sensorId ? SystemMessageActions.UpdateSensor : SystemMessageActions.CreateSensor,
                                data: sensor,
                            } as SystemMessage<Sensor>);
                        }
                    });
    }

    deleteSensor(sensorId: number) {
        const componentRef = this.bottomSheet.open(DeleteSensorComponent);
        componentRef.instance.sensorId = sensorId;
        componentRef.afterDismissed()
                    .pipe(take(1))
                    .subscribe(deleteSensor => {
                        if (!!deleteSensor) {
                            this.messageBus.push({
                                type: SystemMessageActions.DeleteSensor,
                                data: sensorId,
                            } as SystemMessage<number>);
                        }
                    });
    }
}
