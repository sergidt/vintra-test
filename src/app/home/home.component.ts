import { Component, OnInit } from '@angular/core';
import { SystemMessageActions } from '../definitions/system-message-actions';
import { MessageBus } from '../services/message-bus/message-bus.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    constructor(private messageBus: MessageBus) {
    }

    ngOnInit(): void {
        this.messageBus.push({ type: SystemMessageActions.GetAuthenticatedUser });
        this.messageBus.push({ type: SystemMessageActions.GetSensorList });
    }

}
